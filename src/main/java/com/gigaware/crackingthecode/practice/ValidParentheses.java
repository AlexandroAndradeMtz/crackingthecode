package com.gigaware.crackingthecode.practice;

import static com.gigaware.crackingthecode.App.println;

import java.util.Deque;
import java.util.LinkedList;

/**
 * From: https://www.youtube.com/watch?v=xY65bgfXJTk&list=PLRHSCQC2qg1-vaE-wzryb_QM_YvF8NJ5R&index=5
 * 
 * @author Alex Andrade (yngwie_alex@hotmail.com)
 *
 */
public class ValidParentheses {
    
    public static void main(String[] params) {

        var s = "abc ( d ) efg ( h[ ij { kl } m ] n ) p";
        Deque<Character> q = new LinkedList<>();
        
        for ( char c : s.toCharArray() ) {
            if ( isOpening( c ) ) {
                q.push(c);
            } else if ( isClosing( c ) ) {
                if ( q.isEmpty() ) throw new IllegalArgumentException();
                switch( c ) {
                    case ')' : { 
                        if ( q.peek() == '(') { 
                            q.pop(); 
                        } else { throw new IllegalArgumentException(); 
                        } 
                        break;
                    }
                    case '}' : {
                        if ( q.peek() == '{') { 
                            q.pop(); 
                        } else { throw new IllegalArgumentException(); 
                        } 
                        break;

                    }
                    case ']' : {
                        if ( q.peek() == '[') { 
                            q.pop(); 
                        } else { throw new IllegalArgumentException(); 
                        } 
                        break;
                    }
                }
            }
        }
        
        println("All Good !!!");
        
    }
    
    private static boolean isOpening(char c) {
        return "([{".contains( "" + c );
    }
    
    private static boolean isClosing(char c) {
        return ")]}".contains( "" + c );
    }
    

}
