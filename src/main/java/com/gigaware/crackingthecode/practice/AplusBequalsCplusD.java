package com.gigaware.crackingthecode.practice;

import static com.gigaware.crackingthecode.App.println;

/**
 * Example: Print all positive integer solutions to the equation 
 * a3 + b3 = c3 + d3 where a, b, c, and d are integers 
 * between 1 and 1000.
 * 
 * @author Alex Andrade (yngwie_alex@hotmail.com)
 *
 */
public class AplusBequalsCplusD {
    
    public static void main(String[] params) {
        
        int n = 1_000;
        
        for ( int a = 1; a <= n; a++ ) {
            for ( int b = 1; b <= n; b++ ) {
                for ( int c = 1; c <= n; c++ ) {
                    for ( int d = 1; d <= n; d++ ) {
                        if ( Math.pow( a, 3 ) + Math.pow( b, 3 ) == Math.pow( c, 3 ) + Math.pow( d, 3 ) ) {
                            println( "a: " + a + ", b: " + b + ", c: " + c + ", d: " + d );
                            break;
                        }
                    }
                }
            }
        }
    }
}
