package com.gigaware.crackingthecode.practice;

import static com.gigaware.crackingthecode.App.println;

/**
 * @author Alex Andrade (yngwie_alex@hotmail.com)
 */
public class CalculateRootSquare {

    public static void main(String[] params) {
        
        int n = 25;
        double result = rootSquare( n );
        println( "result: " + result );
        
//        println( 3.162277660168379 * 3.162277660168379 );

    }
    
    private static double rootSquare(int n) {
        double r = n * 1.0 / 2.0D;
        double t = r;
        double e = 0.001;
        
        // 9.999 < 10 < 10.001

        do {
            t = half(t);
            if ( r * r  > n ) {
                r -= t;
            } else if ( r * r < n ) {
                r += t;
            }
        } while ( r*r+e < n || r*r-e > n  );
        
        
        return r;
    }
    
    private static double half(double d) { return d / 2.0D; }
}
