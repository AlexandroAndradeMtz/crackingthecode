package com.gigaware.crackingthecode.practice.util;

public class BiNode<T> {

    public T value;
    public BiNode<T> left;
    public BiNode<T> right;

    public BiNode() { }
    public BiNode(T value) { this.value = value; }

}
