package com.gigaware.crackingthecode.practice;

import static com.gigaware.crackingthecode.App.println;

import java.util.HashSet;
/**
 * From Cracking the Code Interview book
 * 
 * Count the number of pairs for given 'k' difference
 *
 * Example: Given an array of distinct integer values, count the number of pairs of integers that
 * have difference k. For example, given the array {1, 7, 5, 9, 2, 12, 3} and the difference
 * k = 2, there are four pairs with difference 2: (1, 3), (3, 5), (5, 7), (7, 9). 
 *
 *
 * @author Alex Andrade (yngwie_alex@hotmail.com)
 * 
 */
public class CountNumberOfPairsForKDifference {
  
    public static void main(String[] params) {
        
        int[] arr = new int[] { 1, 7, 5, 9, 2, 12, 3 };
        int k = 2;
        int count = 0;
        
        var found = new HashSet<Integer>();
        for ( int n : arr ) found.add( n );
        for ( int n : arr ) if ( found.contains( n + k ) ) count++;
        
        
        
        println( "Number of Pairs: " + count );
        
    }
}
