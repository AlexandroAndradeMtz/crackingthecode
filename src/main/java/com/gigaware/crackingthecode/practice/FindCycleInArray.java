package com.gigaware.crackingthecode.practice;

import static com.gigaware.crackingthecode.App.println;

/**
 * From: https://www.youtube.com/watch?v=VX2oZkDJeGA&list=PLRHSCQC2qg1-vaE-wzryb_QM_YvF8NJ5R&index=35
 * 
 * @author Alex Andrade (yngwie_alex@hotmail.com)
 *
 */
public class FindCycleInArray {
    
    public static void main(String[] params) {

        int[] arr = { 3, 1, 10, 10, 10 };
        int it = 0;
        int i = 0;
        
        
        // [ 1, 2, 3, 4, 5 ] - Not Cyclic
        // [ 1, 2, 3, 4, 0 ] - Is Cyclic
        // [ 0 ]             - Is Cyclic
        // [ 1, 1, 10, 10, 10 ] - TRUE
        
        
        while ( it <= arr.length ) {
            if ( i < 0 || i >= arr.length ) { break; }
            
            i = arr[i];
            it++;
        }
        
        println("Is Cyclic?: " + ( it > arr.length ) );

    }
}
