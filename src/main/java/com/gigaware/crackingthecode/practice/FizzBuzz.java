package com.gigaware.crackingthecode.practice;

import static com.gigaware.crackingthecode.App.println;

/**
 * Popular Fizz Buzz game
 * From: https://www.youtube.com/watch?v=QPZ0pIK_wsc
 * 
 * Count in turn: 1, 2, 3, 4
 * ex: 1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11
 * 
 * FizzBuzz for 15 !!!
 * 
 * @author Alex Andrade (yngwie_alex@hotmail.com)
 *
 */
public class FizzBuzz {

    public static void main( String[] params ) {
        
        int n = 100;
        
        for ( int i = 1; i <= n; i++ ) {
            if ( isFizzBuzz( i ) ) {
                println("FizzBuzz ");
            } else if ( isFizz( i ) ) {
                println("Fizz ");
            } else if ( isBuzz( i ) ) {
                println("Buzz ");
            } else {
                println( i + " " );
            }
        }
    }
    
    private static boolean isFizz(int n) {
        return n % 3 == 0;
    }
    
    private static boolean isBuzz(int n) {
        return n % 5 == 0;
    }
    
    private static boolean isFizzBuzz(int n) {
        return isFizz( n ) && isBuzz( n );
    }
}
