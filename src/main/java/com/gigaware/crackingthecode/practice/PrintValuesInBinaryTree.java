package com.gigaware.crackingthecode.practice;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.gigaware.crackingthecode.App;
import com.gigaware.crackingthecode.practice.util.BiNode;

/**
 * From: https://www.youtube.com/watch?v=P_YMAJWEK3Q&list=PLRHSCQC2qg1-vaE-wzryb_QM_YvF8NJ5R&index=14
 * 
 * from:
 * 
 *        2
 *       /  \
 *      3    4
 *     /    / \
 *    1    2   5
 *    
 * @author Alex Andrade (yngwie_alex@hotmail.com)
 *
 */
public class PrintValuesInBinaryTree {

    public static void main(String[] params) {
        
        var root = createTree();
        // 2
        // 34
        // 125
        
        traverse( root );

        map.entrySet()
           .stream()
           .map(Entry::getValue)
           .forEach(App::println);
        
    }
    
    private static Map<Integer, String> map = new LinkedHashMap<>();
    
    private static void traverse( BiNode<Integer> node) { traverse( node, 0 ); }

    private static void traverse( BiNode<Integer> node, int i ) {
        if ( node == null ) return;
        
        var v = node.value;
        if ( !map.containsKey( i ) ) {
            map.put( i, "" + v );
        } else {
            map.replace( i, map.get( i ) + v);
        }
        
        traverse(node.left,  i + 1);
        traverse(node.right, i + 1);
    }
    
    private static BiNode<Integer> createTree() {
        var root = new BiNode<Integer>(2);
        
        var a = new BiNode<Integer>( 3 );
        var b = new BiNode<Integer>( 4 );
        var c = new BiNode<Integer>( 1 );
        var d = new BiNode<Integer>( 2 );
        var e = new BiNode<Integer>( 5 );
        
        root.left = a;
        root.right = b;
        
        a.left = c;
        b.left = d;
        b.right = e;

        return root;
    }

}
