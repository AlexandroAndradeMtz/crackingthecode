package com.gigaware.crackingthecode.practice;

import static com.gigaware.crackingthecode.App.println;

/**
 * From: https://www.youtube.com/watch?v=qRNB8CV3_LU&list=PLRHSCQC2qg1-vaE-wzryb_QM_YvF8NJ5R&index=29
 * 
 * s = "AABCDDBBBEA"
 * o = "BBB"
 * 
 * @author Alex Andrade (yngwie_alex@hotmail.com)
 *
 */
public class LongestConsecutiveCharacter {

    public static void main( String[] params ) {
        
        var s = "AABCDDDDDBBBEABBBB";
        //       ^
        char current   = ' ';
        int  currCount = 0;
        
        char maxChar  = ' ';
        int maxCount = 0;
        
        for ( char c : s.toCharArray() ) {
            if ( c != current ) {
                current = c;
                currCount = 1;
            } else {
                currCount++;
            }
            
            if ( currCount > maxCount ) {
                maxChar  = current;
                maxCount = currCount;
            }
        }
        
        println("Max Char: " + maxChar + ", found: " + maxCount + " times" );
        
        
    }
    
    
}
